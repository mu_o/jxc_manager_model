create database drp;

go
--drop database drp
use drp;
go

--进货表
create table Stock(

StockId int primary key identity(1,1),--商品编号
ItemName nvarchar(20) not null ,--物品名称
Quantity int not null default (0),--数量
Price money not null default (0.00), --单价
SumMoney money not null default (0.00),--总金额
StockTime  smalldatetime default (getdate())--进货时间

)
alter table EnterStock_Detail add constraint FK_EnterStock_Detail_StockId foreign key (StockId) references Stock(StockId);

-- 入库单明细
create table  EnterStock_Detail 
( 
  EnterStock_ID int  primary key identity(1,1),-- 入库单编号 , 主键,
  StockId int not null, --此种商品编号,主键, 外键 (参照 Stock 表 )
  Quantity  int  NOT NULL,  -- 所存数量 
  Price   money   not null, --单价

)

alter table Inventory add constraint FK_Inventory_StockId1 foreign key (InventoryId) references EnterStock_Detail(EnterStock_ID);
--库存表
create table Inventory(

InventoryId int primary key identity (1,1),-- 库存编号 , 主键
StockId int not null,--存货商品编号
InventoryTime smalldatetime default (getdate ())--入库时间
)
alter table StockRemoval add constraint FK_StockRemoval_StockId foreign key (StockRemoval) references Inventory(InventoryId);

--出库表
create table StockRemoval(
StockRemoval int primary key identity (1,1),--出库单编号 , 主键,
StockId int not null,
StockRemovalTime smalldatetime default (getdate())

)

alter table Access add constraint FK_Access_StockId foreign key (AccessId) references EnterStock_Detail(EnterStock_ID);

alter table Access add constraint FK_Access_StockId foreign key (AccessId) references StockRemoval(StockRemoval);

--存取信息表
create table Access(
AccessId int primary key identity(1,1),
StockId int not null,
InventoryQuantity int not null,	--存入数量
StockRemovalQuantity int not null,--取出数量
AccessTime smalldatetime default (getdate())--时间
)

alter table Sell add constraint FK_Sell_StockId foreign key (StockId) references Stock(StockId);
--销售表
create table Sell(
SellId int primary key identity (1,1),-- 销售编号 , 主键
StockId int not null,--销售商品
SellQuantity int not null,--销售数量
SellPrice money not null,--销售单价
SellSumMoney money not null,--销售总金额
SellTime smalldatetime default (getdate ())--销售时间


)

